﻿// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using CoyoteTester.Interfaces;
using Microsoft.Coyote.Coverage;
using Microsoft.Coyote.SmartSockets;

namespace Microsoft.Coyote.SystematicTesting
{
    internal sealed class TestingProcessSchedulerMultivesta : TestingProcessScheduler
    {
        /// <summary>
        /// Configuration.
        /// </summary>
        private readonly Configuration Configuration;

        private TestingProcessMultivesta TestingProcess;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestingProcessScheduler"/> class.
        /// </summary>
        private TestingProcessSchedulerMultivesta(Configuration configuration)
                : base(configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Notifies the testing process scheduler that a bug was found.
        /// </summary>
        private void NotifyBugFound(uint processId)
        {
            string name = "CoyoteTestingProcess." + processId;
            lock (this.Terminating)
            {
                this.Terminating.Add(name);
            }

            lock (this.SchedulerLock)
            {
                if (!this.Configuration.PerformFullExploration && this.BugFoundByProcess is null)
                {
                    Console.WriteLine($"... Task {processId} found a bug.");
                    this.BugFoundByProcess = processId;
                    // must be async relative to this NotifyBugFound handler.
                    Task.Run(() => this.CleanupTestProcesses(processId));
                }
            }
        }

        private async void CleanupTestProcesses(uint bugProcessId, int maxWait = 60000)
        {
            try
            {
                string serverName = this.Configuration.TestingSchedulerEndPoint;
                var stopRequest = new TestServerMessage("TestServerMessage", serverName)
                {
                    Stop = true
                };

                var snapshot = new Dictionary<uint, Process>(this.TestingProcesses);

                foreach (var testingProcess in snapshot)
                {
                    if (testingProcess.Key != bugProcessId)
                    {
                        string name = "CoyoteTestingProcess." + testingProcess.Key;

                        lock (this.Terminating)
                        {
                            this.Terminating.Add(name);
                        }

                        if (this.TestingProcessChannels.TryGetValue(name, out SmartSocketClient client) && client.BackChannel != null)
                        {
                            // use the back channel to stop the client immediately, which will trigger client
                            // to also send us their TestReport (on the regular channel).
                            await client.BackChannel.SendAsync(stopRequest);
                        }
                    }
                }

                await this.WaitForParallelTestReports(maxWait);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"... Exception: {ex.Message}");
            }
        }

        private void KillTestingProcesses()
        {
            lock (this.SchedulerLock)
            {
                foreach (var testingProcess in this.TestingProcesses)
                {
                    try
                    {
                        var process = testingProcess.Value;
                        if (!process.HasExited)
                        {
                            IO.Debug.WriteLine("... Killing child process : " + process.Id);
                            process.Kill();
                            process.Dispose();
                        }
                    }
                    catch (Exception e)
                    {
                        IO.Debug.WriteLine("... Unable to terminate testing task: " + e.Message);
                    }
                }

                this.TestingProcesses.Clear();
            }
        }

        /// <summary>
        /// Sets the test report from the specified process.
        /// </summary>
        private void SetTestReport(TestReport testReport, uint processId)
        {
            lock (this.SchedulerLock)
            {
                this.MergeTestReport(testReport, processId);
            }
        }

        /// <summary>
        /// Creates a new testing process scheduler.
        /// </summary>
        internal static new TestingProcessSchedulerMultivesta Create(Configuration configuration)
        {
            return new TestingProcessSchedulerMultivesta(configuration);
        }

        // CR
        /// <summary>
        /// Runs the Coyote testing scheduler.
        /// </summary>
        internal new void Run()
        {
            Console.WriteLine($"Starting TestingProcessScheduler in process {Process.GetCurrentProcess().Id}");

            // Start the local server.
            this.StartServer();
            this.Profiler.StartMeasuringExecutionTime();
            this.CreateAndRunInMemoryTestingProcess();
            this.Profiler.StopMeasuringExecutionTime();

            // Stop listening and close the server.
            this.StopServer();
            /*
            if (!IsProcessCanceled)
            {
                // Merges and emits the test report.
                this.EmitTestReport();
            }
            */
        }

        private async Task WaitForParallelTestReports(int maxWait = 60000)
        {
            this.LastMessageTime = Environment.TickCount;

            // wait for the parallel tasks to connect to us
            while (this.TestProcessesConnected < this.TestingProcesses.Count)
            {
                await Task.Delay(100);
                this.AssertTestProcessActivity(maxWait);
            }

            // wait 60 seconds for tasks to call back with all their reports and disconnect.
            // and reset the click each time a message is received
            while (this.TestingProcessChannels.Count > 0)
            {
                await Task.Delay(100);
                this.AssertTestProcessActivity(maxWait);
            }
        }

        private void AssertTestProcessActivity(int maxWait)
        {
            if (this.LastMessageTime + maxWait < Environment.TickCount)
            {
                // oh dear, haven't heard from anyone in 60 seconds, and they have not
                // disconnected, so time to get out the sledge hammer and kill them!
                this.KillTestingProcesses();
                throw new Exception("Terminating TestProcesses due to inactivity");
            }
        }

        /// <summary>
        /// Creates and runs an in-memory testing process.
        /// </summary>
        private void CreateAndRunInMemoryTestingProcess()
        {
            // this.TestingProcess = TestingProcess.Create(this.Configuration);
            Console.WriteLine($"... Created '1' testing task.");

            // Runs the testing process.
            this.TestingProcess.Run();

            // Get and merge the test report.
            TestReport testReport = this.TestingProcess.GetTestReport();
            if (testReport != null)
            {
                this.MergeTestReport(testReport, 0);
            }
        }

        /// <summary>
        /// Opens the local server for TestingProcesses to connect to.
        /// If we are not running anything out of process then this does nothing.
        /// </summary>
        private void StartServer()
        {
            if (!this.IsRunOutOfProcess)
            {
                return;
            }

            var resolver = new SmartSocketTypeResolver(typeof(BugFoundMessage),
                                                       typeof(TestReportMessage),
                                                       typeof(TestServerMessage),
                                                       typeof(TestProgressMessage),
                                                       typeof(TestTraceMessage),
                                                       typeof(TestReport),
                                                       typeof(CoverageInfo),
                                                       typeof(Configuration));
            var server = SmartSocketServer.StartServer(this.Configuration.TestingSchedulerEndPoint, resolver, this.Configuration.TestingSchedulerIpAddress);
            server.ClientConnected += this.OnClientConnected;
            server.ClientDisconnected += this.OnClientDisconnected;
            server.BackChannelOpened += this.OnBackChannelOpened;

            // pass this along to the TestingProcesses.
            this.Configuration.TestingSchedulerIpAddress = server.EndPoint.ToString();

            IO.Debug.WriteLine($"... Server listening on '{server.EndPoint}'");

            this.Server = server;
        }

        /// <summary>
        /// Closes the local server, if we have one.
        /// </summary>
        private void StopServer()
        {
            if (this.Server != null)
            {
                this.Server.Stop();
                this.Server = null;
            }
        }

        internal void CreateElements()
        {
            this.TestingProcess = TestingProcessMultivesta.Create(this.Configuration);
            // this.TestingProcessMultivesta.CreateElements();
        }

        internal double GetExecutionTime()
        {
            // return this.Profiler.Results();
            return this.TestingProcess.GetExecutionTime();
        }

        internal int GetTotalBugs()
        {
            // return this.GlobalTestReport.NumOfFoundBugs;
            return this.TestingProcess.TestingEngine.GetRuntime().Scheduler.GetTotalBugs();
        }

        internal int GetTotalSteps()
        {
            // return this.GlobalTestReport.TotalExploredFairSteps;
            return this.TestingProcess.TestingEngine.GetRuntime().GetCurrentSteps();
        }

        internal void SetStepToStep(bool flag)
        {
            this.TestingProcess.TestingEngine.GetRuntime().Scheduler.SetStepToStep(flag);
        }

        internal bool GetHasNext()
        {
            return this.TestingProcess.TestingEngine.GetRuntime().Scheduler.GetHasNext();
        }

        internal Task<int> SetToken()
        {
            return this.TestingProcess.TestingEngine.GetRuntime().SetToken();
        }

        internal Task<int> GetToken()
        {
            return this.TestingProcess.TestingEngine.GetRuntime().GetToken();
        }

        internal bool IsRunning()
        {
            return this.TestingProcess.TestingEngine.GetRuntime().Scheduler.GetIsRunning();
        }
    }
}
