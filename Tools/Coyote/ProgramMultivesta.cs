﻿// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Coyote.IO;
using Microsoft.Coyote.SystematicTesting;
using Microsoft.Coyote.Utilities;

namespace Microsoft.Coyote
{
    /// <summary>
    /// Entry point to the Coyote tool.
    /// </summary>
    public sealed class ProgramMultivesta
    {
        private static Configuration Configuration;

        private static TextWriter StdOut;
        private static TextWriter StdError;

        // private static TestingProcessMultivesta testingProcess;
        private static TestingProcessSchedulerMultivesta testingProcessScheduler;

        private static readonly object ConsoleLock = new object();

        public static void Run()
        {
            // Save these so we can force output to happen even if TestingProcess has re-routed it.
            StdOut = Console.Out;
            StdError = Console.Error;

            AppDomain.CurrentDomain.ProcessExit += OnProcessExit;
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            Console.CancelKeyPress += OnProcessCanceled;

            RunTest();
        }

        private static void RunTest()
        {
            if (Configuration.ReportCodeCoverage || Configuration.ReportActivityCoverage)
            {
                // This has to be here because both forms of coverage require it.
                CodeCoverageInstrumentation.SetOutputDirectory(Configuration, makeHistory: true);
            }

            if (Configuration.ReportCodeCoverage)
            {
#if NETFRAMEWORK
                // Instruments the program under test for code coverage.
                CodeCoverageInstrumentation.Instrument(Configuration);

                // Starts monitoring for code coverage.
                CodeCoverageMonitor.Start(Configuration);
#endif
            }

            Console.WriteLine(". Testing " + Configuration.AssemblyToBeAnalyzed);
            if (!string.IsNullOrEmpty(Configuration.TestMethodName))
            {
                Console.WriteLine("... Method {0}", Configuration.TestMethodName);
            }

            testingProcessScheduler.Run();
            Console.WriteLine("*");
        }

        /// <summary>
        /// Callback invoked when the current process terminates.
        /// </summary>
        private static void OnProcessExit(object sender, EventArgs e) => Shutdown();

        /// <summary>
        /// Callback invoked when the current process is canceled.
        /// </summary>
        private static void OnProcessCanceled(object sender, EventArgs e)
        {
            if (!TestingProcessSchedulerMultivesta.IsProcessCanceled)
            {
                TestingProcessSchedulerMultivesta.IsProcessCanceled = true;
                Shutdown();
            }
        }

        /// <summary>
        /// Callback invoked when an unhandled exception occurs.
        /// </summary>
        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            ReportUnhandledException((Exception)args.ExceptionObject);
            Environment.Exit(1);
        }

        private static void ReportUnhandledException(Exception ex)
        {
            Console.SetOut(StdOut);
            Console.SetError(StdError);

            PrintException(ex);
            for (var inner = ex.InnerException; inner != null; inner = inner.InnerException)
            {
                PrintException(inner);
            }
        }

        private static void PrintException(Exception ex)
        {
            lock (ConsoleLock)
            {
                if (ex is ExecutionCanceledException)
                {
                    Error.Report("[CoyoteTester] unhandled exception: {0}: {1}", ex.GetType().ToString(),
                        "This can mean you have a code path that is not controlled by the runtime that threw an unhandled exception. " +
                        "This typically happens when you create a 'System.Threading.Tasks.Task' instead of 'Microsoft.Coyote.Tasks.Task' " +
                        "or create a 'Task' inside a 'StateMachine' handler. One known issue that causes this is using 'async void' " +
                        "methods, which is not supported.");
                    StdOut.WriteLine(ex.StackTrace);
                }
                else
                {
                    Error.Report("[CoyoteTester] unhandled exception: {0}: {1}", ex.GetType().ToString(), ex.Message);
                    StdOut.WriteLine(ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Shutdowns any active monitors.
        /// </summary>
        private static void Shutdown()
        {
#if NETFRAMEWORK
            if (Configuration != null && Configuration.ReportCodeCoverage && CodeCoverageMonitor.IsRunning)
            {
                Console.WriteLine(". Shutting down the code coverage monitor, this may take a few seconds...");

                // Stops monitoring for code coverage.
                CodeCoverageMonitor.Stop();
                CodeCoverageInstrumentation.Restore();
            }
#endif
        }

        public static void SetConfiguration(Configuration config)
        {
            Configuration = config;
        }

        public static void SetRandomGeneratorSeed(uint seed)
        {
            Configuration.WithRandomGeneratorSeed(seed);
        }

        public static double GetExecutionTime()
        {
            return testingProcessScheduler.GetExecutionTime();
        }

        public static double GetTotalBugs()
        {
            return testingProcessScheduler.GetTotalBugs();
        }

        public static double GetTotalSteps()
        {
            return testingProcessScheduler.GetTotalSteps();
        }

        public static void SetStepToStep(bool flag)
        {
            testingProcessScheduler.SetStepToStep(flag);
        }

        public static Task<int> SetToken()
        {
            return testingProcessScheduler.SetToken();
        }

        public static Task<int> GetToken()
        {
            return testingProcessScheduler.GetToken();
        }

        public static bool GetHasNext()
        {
            return testingProcessScheduler.GetHasNext();
        }

        public static void CreateElements()
        {
            testingProcessScheduler = TestingProcessSchedulerMultivesta.Create(Configuration);
            testingProcessScheduler.CreateElements();
        }

        public static bool IsRunning()
        {
            return testingProcessScheduler.IsRunning();
        }
    }
}
