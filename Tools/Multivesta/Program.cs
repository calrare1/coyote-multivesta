﻿// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Coyote;

public static class CoyoteMultivesta
{
  // public CoyoteMultivesta(){ }

  public static int TOTALTIME = 0;
  public static int TOTALSTEPS = 1;
  public static int TOTALBUGS = 2;
  public static Task T;
  public static bool Started = false;

  public static void StartSimulation()
  {
    ProgramMultivesta.CreateElements();
    T = new Task(() =>
    {
       ProgramMultivesta.Run();
    });
    T.Start();
    Started = true;
    Console.WriteLine("*");
  }

  public static void SetSimulatorForNewSimulation(uint seed)
  {
    ProgramMultivesta.SetRandomGeneratorSeed(seed);
    CoyoteMultivesta.StartSimulation();
  }

  public static void PerformWholeSimulation()
  {
    ProgramMultivesta.SetStepToStep(false);
    CoyoteMultivesta.PerformOneStepOfSimulation();
  }

  public static void PerformWholeSimulation2()
  {
    while (!CoyoteMultivesta.GetIsSimulationFinished())
    {
       CoyoteMultivesta.PerformOneStepOfSimulation();
    }
  }

  public static void PerformOneStepOfSimulation()
  {
    if (!CoyoteMultivesta.GetIsSimulationFinished())
    {
       Task task = new Task(() =>
       {
          ProgramMultivesta.SetToken();
       });
       task.Start();
       task.Wait();
       task.Dispose();
    }
  }

  /*
  public static bool GetIsSimulationFinished()
  {
    var flag = false;
    Task task = new Task(async () =>
    {
        flag = ProgramMultivesta.GetHasNext();
        await Task.Delay(0);
    });
    task.Start();
    task.Wait();
    return !flag;
  }
  */

  public static bool GetIsSimulationFinished()
  {
    return !ProgramMultivesta.GetHasNext();
  }

  public static double Rval(int observation)
  {
    double ans = 0.0;
    if (observation == TOTALTIME)
    {
      ans = ProgramMultivesta.GetExecutionTime();
    }
    else if (observation == TOTALBUGS)
    {
      ans = ProgramMultivesta.GetTotalBugs();
    }
    else if (observation == TOTALSTEPS)
    {
      ans = ProgramMultivesta.GetTotalSteps();
    }

    return ans;
  }

  public static Configuration CreateConfiguration(string model, string method, uint seed)
  {
    var configuration = Configuration.Create();
    configuration.WithAssembly(model);
    configuration.WithTestMethodName(method);
    configuration.WithRandomGeneratorSeed(seed);
    return configuration;
  }

  public static void Main(string[] args)
  {
    ProgramMultivesta.SetConfiguration(CoyoteMultivesta.CreateConfiguration(args[0], args[1], uint.Parse(args[2])));
    // ProgramMultivesta.CreateElements();
    CoyoteMultivesta.StartSimulation();
    // Console.WriteLine("*");

    var ops = Console.ReadLine().Split(' ');
    var op = short.Parse(ops[0]);
    while (op != 0)
    {
      if (op == 3)
      {
        CoyoteMultivesta.PerformOneStepOfSimulation();
      }
      else if (op == 4)
      {
        var flag = CoyoteMultivesta.GetIsSimulationFinished();
        Console.WriteLine(flag);
        Console.WriteLine("+");
      }
      else if (op == 5)
      {
        // CoyoteMultivesta.PerformWholeSimulation2();
        CoyoteMultivesta.PerformWholeSimulation();
        T.Wait();
      }
      else if (op == 6)
      {
         Console.WriteLine($"{CoyoteMultivesta.Rval(TOTALTIME)}");
         Console.WriteLine("+");
      }
      else if (op == 7)
      {
         Console.WriteLine($"{CoyoteMultivesta.Rval(TOTALBUGS)}");
         Console.WriteLine("+");
      }
      else if (op == 8)
      {
         Console.WriteLine($"{CoyoteMultivesta.Rval(TOTALSTEPS)}");
         Console.WriteLine("+");
      }

      ops = Console.ReadLine().Split(' ');
      op = short.Parse(ops[0]);
    }

    Console.WriteLine("end");
  }
}
